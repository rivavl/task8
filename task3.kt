import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(3) { i ->
                println("I'm sleeping $i")
                delay(1000L)
            }
        } finally {
            println("I'm running finally")
        }
    }
    delay(3000L)
    println("main: I'm tired of waiting!")
    job.join()
    println("main: Now I can quit.»")
}

/////////////////////////////
//«I'm sleeping 0 ...
//I'm sleeping 1 ...
//I'm sleeping 2 ...
//main: I'm tired of waiting!
//I'm running finally
//main: Now I can quit.»
//////////////////////////////
