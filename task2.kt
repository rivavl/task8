import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
    * У меня получилось следующее:
    * синхронный вызов 2546 мс
    * асинхронный - 1531 мс
    *
    * это объясняется тем, что при синхронном вызове мы ждем, когда завершится первый метод,
    * потом стартуем второй при асинхронном - мы не ждем завершения первого метода для старта
    * второго, таким образом сокращается время работы
    * */

fun main(): Unit = runBlocking {

    launch {
        val timeInMillis = System.currentTimeMillis()
        val firstNum = async { foo1() }
        val secondNum = async { foo2() }
        println("${firstNum.await() + secondNum.await()}")
        println("Functions time async: ${System.currentTimeMillis() - timeInMillis}")
    }

    launch {
        val timeInMillis = System.currentTimeMillis()
        val firstNum = foo1()
        val secondNum = foo2()
        println("${firstNum + secondNum}")
        println("Functions time: ${System.currentTimeMillis() - timeInMillis}")
    }
}

suspend fun foo1(): Int {
    println("foo1")
    delay(1000L)
    return 111
}

suspend fun foo2(): Int {
    println("foo2")
    delay(1500L)
    return 222
}

